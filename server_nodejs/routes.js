var Types = require('hapi').types;
var Joi = require('Joi');

/* Routes */
module.exports = [{
    method: 'GET',
    path: '/products',
    config: {
        handler: getProducts
    }
}, {
    method: 'GET',
    path: '/places',
    config: {
        handler: getPlaces,
    }
}, {
    method: 'GET',
    path: '/places/{id}',
    config: {
        handler: getPlace
    }
}];

/* DB */
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('memory.db');

/**
* columns - columns to select
* table - table to select from
* whereStatement - where statement for the query
* request - the request variable if it exists in the parent function
* callbackFUnction - the name of the function to run after the query finishes
*/
function selectRowsFromDatabase(columns, table, whereStatement, request, callbackFunction) {

    db.serialize(function() {
        var query = "";

        if(whereStatement.length != 0)
            query = "SELECT " + columns + " FROM " + table + " WHERE " + whereStatement;
        else
            query = "SELECT " + columns + " FROM " + table;

        db.all(query, function(err, rows) {
            callbackFunction(rows, request);
        });
    });

}

/* Callback functions */
function replyInJSon(rows, request) {
    console.log(rows);
    request.reply(rows);
}


/* Controllers */

//get one place - /places/ID
function getPlace(request) {
    if (!isNaN(request.params.id)) {
        selectRowsFromDatabase("id,placeID,rating", "Lugar", "id = "+request.params.id, request, replyInJSon);
    }
    else {
        request.reply(null);
    }    
}

function startDB() {
    db.serialize(function() {
     db.run("CREATE TABLE IF NOT EXISTS Utilizador(id INTEGER PRIMARY KEY AUTOINCREMENT,username TEXT UNIQUE NOT NULL,email TEXT UNIQUE NOT NULL,password VARCHAR(255) NOT NULL,salt VARCHAR(255) NOT NULL,userType INTEGER NOT NULL)");
     db.run("CREATE TABLE IF NOT EXISTS Lugar(id INTEGER PRIMARY KEY AUTOINCREMENT,placeID VARCHAR(255) NOT NULL,rating DOUBLE NOT NULL)");
     db.run("CREATE TABLE IF NOT EXISTS UtilizadorOpiniao(id INTEGER PRIMARY KEY AUTOINCREMENT,placeID VARCHAR(255),userID INTEGER,rating INTEGER NOT NULL,comment VARCHAR(50) NOT NULL,date DATE NOT NULL,FOREIGN KEY(userID) REFERENCES utilizadores(id),FOREIGN KEY(placeID) REFERENCES lugares(placeID))");         
   
    var stmt = db.prepare("INSERT INTO Utilizador (username,email,password,salt,userType) VALUES (?,?,?,?,1)");
      for (var i = 0; i < 2; i++) {
          stmt.run("user"+i,"mail"+i,"pass"+i,"salt"+i);
      }
      stmt.finalize();

    var stmt = db.prepare("INSERT INTO Lugar (placeID,rating) VALUES (?,0)");
      for (var i = 0; i < 2; i++) {
          stmt.run("place"+i);
      }
      stmt.finalize();
    });
};


//get all places - /places
function getPlaces(request) {
    selectRowsFromDatabase("*", "Lugar", "", request, replyInJSon);
}

//get user
function getUser(request) {
    if (!isNaN(request.params.id)) {
        selectRowsFromDatabase("id,placeID,rating", "Lugar", "id = "+request.params.id, request, replyInJSon);
    }
    else {
        request.reply(null);
    }    
}

function getProducts(request) {
    startDB();
}