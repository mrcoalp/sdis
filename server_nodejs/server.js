var Hapi = require('hapi');
var routes = require('./routes');

var serverConfig = {
    cors: {
        origin: ['http://localhost:8100', 'http://localhost:8080']
    }
};

var server = new Hapi.Server('0.0.0.0', 8080, serverConfig);

server.pack.require({ lout: { endpoint: '/docs' } }, function (err) {

    if (err) {
        console.log('Failed loading plugins');
    }
});

server.addRoutes(routes);

server.start();